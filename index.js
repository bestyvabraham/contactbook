const ul = document.getElementById('contactlist')
const form = document.getElementById('form')
form.addEventListener('submit', handleformsub)

function handleformsub(event){
    event.preventDefault()

    const name = form['name'].value
    const phone = form['phone'].value
    const image = form['image'].value

    if(!name){
    console.log('name cannot be null')
    return
    }
    else if (name.trim() ===""){
        console.log("name cannot be just spaces")
        return
    }

    const contact = createcontact(name, phone, image)
    ul.appendChild(contact)
}

function createcontact(name, phone, image){

    const li = document.createElement('li')
    const img = document.createElement('img')
    img.src = image
    li.appendChild(img)

    const div = document.createElement('div')
    li.appendChild(div)

    const h3 = document.createElement('h3')
    h3.innerHTML = name
    div.appendChild(h3)

    const span = document.createElement('span')
    span.innerHTML = phone
    div.appendChild(span)

    return li
}



